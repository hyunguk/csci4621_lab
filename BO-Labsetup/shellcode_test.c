#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc < 2){
        printf("Usage: ./shellcode_test [shellcode-file]\n");
        exit(1);
    }
    unsigned char shellcode[500];
    FILE *fp = fopen(argv[1], "rb");
    fread(shellcode, sizeof(shellcode), 1, fp);

    int (*func)() = (int(*)())shellcode;
    func();
    fclose(fp);

    return 0;
}
