#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void foo(char* str)
{
    char buf[300];
    strcpy(buf, str);
    printf("%s\n", buf);
}

int main(int argc, char** argv)
{
    if (argc < 2){
        printf("Usage: ./vul [file name]\n");
        exit(1);
    }
    char str[517];
    FILE *fp = fopen(argv[1], "r");
    fread(str, sizeof(char), 517, fp);
    foo(str);
    return 0;
}
